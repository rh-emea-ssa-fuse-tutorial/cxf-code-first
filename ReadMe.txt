TUTORIAL
========

	How to create a CXF service (code-first)
	(from scratch, with a text editor, based on Fuse 6.2.1)


	Index:
	-----
		1. Implementation and Deployment in Fuse
		2. Running standalone
		3. Alternative CXF implementation


1. Implementation and Deployment
   (and testing using SoapUI)
=================================

1.1 Create skeleton

	mvn archetype:generate                   \
	  -DarchetypeGroupId=org.apache.camel.archetypes  \
	  -DarchetypeArtifactId=camel-archetype-blueprint \
	  -DarchetypeVersion=2.15.1.redhat-621084  \
	  -DgroupId=learn.cxf                  \
	  -DartifactId=cxf-code-first

	(there is a bug in the JUnit, remove the slash in "/OSGI-INF...")

	Note: the archetype will generate some code that may enter in conflict
		  with our tutorial, simply remove the non-relevant code.


1.2 Create the Web Service Interface:

	----------------------------------
	package learn.cxf;
	import javax.jws.*;

	@WebService(name="HelloWS")
	public interface Hello
	{
	    String hello(@WebParam(name="thename") String thename);
	}
	----------------------------------


1.3 Add the WS plugin to auto-generate the WSDL

      <plugin>
		<groupId>org.apache.cxf</groupId>
        <artifactId>cxf-java2ws-plugin</artifactId>
        <version>3.0.4.redhat-621084</version>
		<executions>
		  <execution>
			<id>process-classes</id>
			<phase>process-classes</phase>
			<configuration>
			<className>learn.cxf.Hello</className>
			<genWsdl>true</genWsdl>
			<verbose>true</verbose>
			</configuration>
			<goals>
			  <goal>java2ws</goal>
			</goals>
		  </execution>
		</executions>
      </plugin>


1.4 Add the endpoint in Blueprint

	a. Add the namespace:  
		xmlns:cxf="http://camel.apache.org/schema/blueprint/cxf"

	b. and the endpoint:

		<cxf:cxfEndpoint id="mycxf"
			address="http://localhost:9000/mycxfserver/hello"
			serviceClass="learn.cxf.Hello"/>

		Notes about 'address':
			- its value will overwrite the one defined in the WSDL's port.
			- it requires 'host:port' if run standalone (when testing or with 'camel:run')
			- when deployed in Fuse, you can omit 'host:port' and it will default to:
				> http://localhost:8181/cxf/mycxfserver/hello


1.5 Add the Camel CXF route to consume traffic

		<route id="my-cxf-route">
			<from uri="cxf:bean:mycxf"/>
			<log message="got message ${body}"/>
			<setBody>
				<simple>Hello ${body}</simple>
			</setBody>
		</route>


1.6 Install in Maven and deploy in Fuse (and start)

	a. mvn clean install -DskipTests=true

	b. JBossFuse:karaf@root> install -s mvn:learn.cxf/cxf-code-first/1.0.0


1.7 From a browser, check it got deployed:

	With the address:
	http://localhost:8181/cxf

	It should show:
	Available SOAP services:
	HelloWS
	    hello
		Endpoint address: http://localhost:9000/mycxfserver/hello
		WSDL : {http://cxf.learn/}HelloService
		Target namespace: http://cxf.learn/


1.8 From SoapUI, you should be able to test it:

	a. Select: File > New Soap Project
	b. Give it a name  
	c. Enter 'Initial WSDL' as:
		> http://localhost:9000/mycxfserver/hello?wsdl



2. Testing JUnits and running standalone
========================================

	JBoss Fuse (the engine) already contains all the dependencies required by the project.
	But the project won't run without a number of dependencies when:
		a. running JUnits (maven test phase)
		b. running standalone (with 'mvn camel:run')


2.1 The POM file needs some CXF dependencies:

	a. add dependency ASM at the top of <dependencies>

		<dependency>
		  <groupId>org.ow2.asm</groupId>
		  <artifactId>asm-all</artifactId>
		  <version>4.1</version>
		</dependency>

		(ref: https://access.redhat.com/solutions/1128593)

	b. add CXF dependencies:

		...
			<camel.version>2.15.1.redhat-621084</camel.version>
			<cxf.version>3.0.4.redhat-621084</cxf.version>
		</properties>
		...
		<dependency>
		    <groupId>org.apache.camel</groupId>
      		<artifactId>camel-cxf</artifactId>
		    <version>${camel.version}</version>
		</dependency>
		<dependency>
		    <groupId>org.apache.cxf</groupId>
		    <artifactId>cxf-rt-transports-http</artifactId>
		    <version>${cxf.version}</version>
		</dependency>
		<dependency>
		    <groupId>org.apache.cxf</groupId>
		    <artifactId>cxf-rt-transports-http-jetty</artifactId>
		    <version>${cxf.version}</version>
		</dependency>

2.2 To run standalone:

	a. run the command:
		> mvn clean camel:run

	b. from SoapUI you should be able to hit the service via the URL:
		> http://localhost:9000/mycxfserver/hello


2.3 To run Junits

	a. add the following JUnit that uses the CXF endpoint to send a WS request:

		@Test
		public void testRouteCxf() throws Exception{
			Object response = template.sendBody("cxf:bean:mycxf", ExchangePattern.InOut, "junit");
			assertEquals("ups", "[Hello junit]", response.toString());
		}

		Note: CXF returns a list where the response is contained, hence the brackets in the assertion. 

	b. run the maven command:
		> mvn clean test


3. Alternative implementation for the CXF consumer
==================================================

	It might be very interesting to decouple the CXF processing from the transport layer.
	Some reasons why this might be useful:
		> gain more control over the HTTP layer
		> use other transports other than HTTP


3.1 Implementation

	As an example, we could implement the route using the transport Netty as follows:

		<route id="my-netty-cxf-route">
			<from uri="netty-http:http://localhost:8000/mycxfserver/hello"/>
			<to uri="cxfbean:hellobean"/>
		</route>

	where 'hellobean' is defined in blueprint as follows:
		
		<bean id="hellobean" class="learn.cxf.HelloBean"/>

	
	The implementation class 'HelloBean' would look as follows:

		----------------------------------
		@javax.jws.WebService(endpointInterface="learn.cxf.Hello")
		public class HelloBean implements Hello{
			public String hello(String thename){
				System.out.println("bean WS got message: " + thename);
				return "Hello " + thename;
			}
		}
		----------------------------------

	The use of Camel Netty Http would need to include the Maven dependency:

		<dependency>
		  <groupId>org.apache.camel</groupId>
		  <artifactId>camel-netty-http</artifactId>
		  <version>${camel.version}</version>
		</dependency>


3.2 To run standalone:

	a. run the command:
		> mvn clean camel:run

	b. you could equally obtain the WSDL via
		> http://localhost:8000/mycxfserver/hello?wsdl

	c. from SoapUI you should be able to hit the service via the URL:
		> http://localhost:8000/mycxfserver/hello


3.3 To run Junits

	a. add the following JUnit to send a WS request:

		@Test
		public void testRouteNetty() throws Exception
		{
			//prepare new request to destination Netty endpoint
			DefaultExchange request = new DefaultExchange(context);
			request.getIn().setHeader("CamelDestinationOverrideUrl", "http://localhost:8000/mycxfserver/hello");
			request.getIn().setBody("junit");
		
			//trigger the request via CXF to Netty
			Exchange response = template.send("cxf:bean:mycxf", request);

			//expect the 'hello' message
			assertEquals("ups", "Hello junit", response.getOut().getBody(String.class));
		}

	b. run the maven command:
		> mvn clean test
	

