package learn.cxf;


	import javax.jws.*;
	@WebService(name="HelloWS")
	public interface Hello
	{
	    String hello(@WebParam(name="thename") String thename);
	}
