package learn.cxf;


@javax.jws.WebService(endpointInterface="learn.cxf.Hello")
public class HelloBean implements Hello
{
    public String hello(String thename)
    {
		System.out.println("bean WS got message: " + thename);
		return "Hello " + thename;
    }
}
