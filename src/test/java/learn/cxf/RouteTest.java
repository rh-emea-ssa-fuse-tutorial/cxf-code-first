package learn.cxf;

import org.apache.camel.ExchangePattern;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultExchange;
import org.apache.camel.test.blueprint.CamelBlueprintTestSupport;

import org.junit.Test;
import org.junit.Ignore;

public class RouteTest extends CamelBlueprintTestSupport {
	
    @Override
    protected String getBlueprintDescriptor() {
        return "OSGI-INF/blueprint/blueprint.xml";
    }

	@Test
    public void testRouteCxf() throws Exception
	{
		//send a WS request
		Object response = template.sendBody("cxf:bean:mycxf", ExchangePattern.InOut, "junit");

		//expect the 'hello' message
		assertEquals("ups", "[Hello junit]", response.toString());
    }

	@Test
    public void testRouteNetty() throws Exception
	{
		//prepare new request to Netty endpoint
		DefaultExchange request = new DefaultExchange(context);
		request.getIn().setHeader("CamelDestinationOverrideUrl", "http://localhost:8000/mycxfserver/hello");
		request.getIn().setBody("junit");
		
		//trigger the request
		Exchange response = template.send("cxf:bean:mycxf", request);

		//expect the 'hello' message
		assertEquals("ups", "Hello junit", response.getOut().getBody(String.class));
    }

}
